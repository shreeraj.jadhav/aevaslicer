/*==============================================================================

  Copyright (c) Kitware, Inc.

  See http://www.slicer.org/copyright/copyright.txt for details.

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

  This file was originally developed by Julien Finet, Kitware, Inc.
  and was partially funded by NIH grant 3P41RR013218-12S1

==============================================================================*/

#ifndef __qAevaSlicerAppMainWindow_h
#define __qAevaSlicerAppMainWindow_h

// AevaSlicer includes
#include "qAevaSlicerAppExport.h"
class qAevaSlicerAppMainWindowPrivate;

// Slicer includes
#include "qSlicerMainWindow.h"

class Q_AEVASLICER_APP_EXPORT qAevaSlicerAppMainWindow : public qSlicerMainWindow
{
  Q_OBJECT
public:
  typedef qSlicerMainWindow Superclass;

  qAevaSlicerAppMainWindow(QWidget *parent=0);
  virtual ~qAevaSlicerAppMainWindow();

public slots:
  void on_HelpAboutAevaSlicerAppAction_triggered();

protected:
  qAevaSlicerAppMainWindow(qAevaSlicerAppMainWindowPrivate* pimpl, QWidget* parent);

private:
  Q_DECLARE_PRIVATE(qAevaSlicerAppMainWindow);
  Q_DISABLE_COPY(qAevaSlicerAppMainWindow);
};

#endif
