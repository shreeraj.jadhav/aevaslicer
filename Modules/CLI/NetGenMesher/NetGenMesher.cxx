
#include "vtkCleanPolyData.h"
#include "vtkNew.h"
#include "vtkTriangleFilter.h"
#include "vtkUnstructuredGrid.h"
#include "vtkUnstructuredGridWriter.h"
#include "vtkNetGenVolume.h"
#include "vtkSuperquadricSource.h"
#include "vtkDebugLeaks.h"
// MRML includes
#include "vtkMRMLModelNode.h"
#include "vtkMRMLModelStorageNode.h"
#include "NetGenMesherCLP.h"

// Use an anonymous namespace to keep class types and function names
// from colliding when module is used as shared object module.  Every
// thing should be in an anonymous namespace except for the module
// entry point, e.g. main()
//


int main( int argc, char * argv[] )
{
  PARSE_ARGS;
  int status = 0;
  vtkNew<vtkMRMLModelStorageNode> model1StorageNode;
  model1StorageNode->SetFileName(inputModel.c_str());
  vtkNew<vtkMRMLModelNode> model1Node;
  if (!model1StorageNode->ReadData(model1Node))
  {
    std::cerr << "Failed to read input model 1 file " << inputModel << std::endl;
    return EXIT_FAILURE;
  }
  vtkNew<vtkTriangleFilter> filter;
  vtkNew<vtkCleanPolyData> merger;
  vtkNew<vtkNetGenVolume> mesher;
  mesher->SetMaxGlobalMeshSize(maxGlobalMeshSize);
  mesher->SetMeshDensity(meshDensity);
  if (useSecondOrder)
  {
    mesher->SetSecondOrder(1);
  }
  else
  {
    mesher->SetSecondOrder(0);
  }
  mesher->SetNumberOfOptimizeSteps(numOptimizeSteps);
  vtkNew<vtkUnstructuredGridWriter> writer;  
  filter->SetInputConnection(model1Node->GetPolyDataConnection());
  merger->SetInputConnection(filter->GetOutputPort());
  mesher->SetInputConnection(merger->GetOutputPort());
  mesher->Update();
  vtkNew<vtkMRMLModelNode> outputModelNode;
  outputModelNode->SetAndObserveMesh(mesher->GetOutput());
  vtkNew<vtkMRMLModelStorageNode> outputModelStorageNode;
  outputModelStorageNode->SetFileName(outputModel.c_str());
  if (!outputModelStorageNode->WriteData(outputModelNode))
  {
    std::cerr << "Failed to write output model file " << std::endl;
    return EXIT_FAILURE;
  }
  std::cerr << "Mesher completed" << std::endl;
  auto* mesh = mesher->GetOutput();
  if (!mesh)
  {
    std::cerr << "No output.\n";
    status = 1;
    return status;
  } 

  return EXIT_SUCCESS;
}
